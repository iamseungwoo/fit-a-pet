import styled from "styled-components";


const CreateCycleContainer = styled.div`
    width:375px;
    height:812px;
    display:flex;
    flex-direction : column;
    gap:10px;
    margin:0 auto;
    background:#FFFFFF;
`;

export default CreateCycleContainer;