import styled from "styled-components";


const PetAddContainer = styled.div`
    width:375px;
    gap:10px;
    height:812px;
    display:flex;
    flex-direction : column;
    margin-left: auto; 
    margin-right: auto;
    .userImage{
        padding:10px;
    }
`;

export default PetAddContainer;